#include "rclcpp/rclcpp.hpp"
#include "crane_interfaces/msg/crane_reference.hpp" // Correct path to your custom message
#include <random>

class CraneController : public rclcpp::Node {
public:
    CraneController() : Node("crane_controller") {
        // Initialize publisher with the topic name, "crane_reference_topic", and queue size of 10
        publisher_ = this->create_publisher<crane_interfaces::msg::CraneReference>("crane_reference_topic", 10);
        
        // Create a timer with a callback function to publish messages every second
        timer_ = this->create_wall_timer(
            std::chrono::seconds(1),
            std::bind(&CraneController::publish_message, this));
    }

private:
    void publish_message() {
        auto message = crane_interfaces::msg::CraneReference();
        message.lars_test = "hello_world"; // Assign the string value
        message.crane_cylinder_velocity_reference = generate_random_velocity(); // Assign random velocity

        RCLCPP_INFO(this->get_logger(), "Publishing: '%s', %f", message.lars_test.c_str(), message.crane_cylinder_velocity_reference);
        publisher_->publish(message);
    }

    double generate_random_velocity() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(-5.0, 5.0); // Example range, adjust as needed
        return dis(gen);
    }

    rclcpp::Publisher<crane_interfaces::msg::CraneReference>::SharedPtr publisher_;
    rclcpp::TimerBase::SharedPtr timer_;
};

int main(int argc, char * argv[]) {
    rclcpp::init(argc, argv);
    auto node = std::make_shared<CraneController>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}