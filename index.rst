.. Robot documentation master file, created by
   sphinx-quickstart on Thu Feb 24 20:20:05 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MAS418 project documentation!
=================================

.. image:: src/fig/mas418_project.png
 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/example1
   src/example2

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
