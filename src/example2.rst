Example Page 2
==================

Header
------
Math with LaTeX syntax

.. math::
	r_{g,0} = [x, y, z]^T + R_x(\phi) R_y(\theta) R_z(\psi) [0, 0, -d_g]^T
	
Gif example from 2023 

	
.. figure:: fig/GifExample.gif

Go to e.g., this link to convert video to gif: https://ezgif.com/video-to-gif


